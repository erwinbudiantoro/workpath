<?php include('header.php'); ?>
<!-- Header Area-->
    <div class="header-area" id="headerArea">
      <div class="container">
        <!-- Header Content-->
        <div class="header-content position-relative d-flex align-items-center justify-content-between">
          <!-- Back Button-->
          <div class="back-button"><a href="pages.html"><svg width="32" height="32" viewBox="0 0 16 16" class="bi bi-arrow-left-short" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
          </svg></a></div>
          <!-- Page Title-->
          <div class="page-heading">
            <h6 class="mb-0">Tandai Kehadiran</h6>
          </div>
          <!-- Settings-->
          <div class="setting-wrapper"> </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="pt55">
          <div id="preview"></div>

          <div id="response_status" style="display:none;">
            <div class="alert alert-light">
              <div id="berhasil" style="display:none;">Proses Berhasil <svg width="18" height="18" viewBox="0 0 16 16" class="bi bi-check-circle-fill text-success" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
              </svg></div>
              <div class="alamat" id="alamat"></div>
              <div class="tanggal" id="tanggal"></div>
              <a class="btn btn-info mt-3 w-100" href="page-home.php">Ok</a>
            </div>
          </div>

          <div id="take">
              <video autoplay playsinline id="vidStream" class="hidden video_preview"></video>
            <div class="btn_take">
              <div id="swipe_cam" class="swipe_cam">
                <div class="register-form">
                  <button type="button" class="btn btn-default w-100" onclick="swipeCam()"><i class="fa fa-refresh"></i></button>
                </div>
              </div>
              <div id="take_pict" class="take_pict">
                <div class="register-form">
                  <button type="button" class="btn btn-default w-100" id="takePhoto"><i class="fa fa-camera"></i> Take Photo</button>
                </div>
              </div>
            </div>
          </div>
          <div class="analizing_face" id="loader" align="center">
            <small><i class="blink_me">Please wait, our A.I is analyzing your face</i></small>
            <div class="dot-loader">
              <div class="dot1"></div>
              <div class="dot2"></div>
              <div class="dot3"></div>
            </div>
          </div>
           <input type="hidden" id="foto" name="foto" />
           <input type="hidden" id="latitude" name="latitude" />
           <input type="hidden" id="longitude" name="longitude" />
          <img>
      </div>
    </div>
<div id="listcam"></div>

<script>
navigator.geolocation.getCurrentPosition(showPosition);
function showPosition(position) {
  $("#latitude").val(position.coords.latitude);
  $("#longitude").val(position.coords.longitude);
}

function sendData()
{
  var lat = $("#latitude").val();
  var long = $("#longitude").val();
  var foto = $("#foto").val();
  $.ajax(
    {
    type: "post",
    dataType: "json",
    url: '<?php echo "https://". $_SERVER['SERVER_NAME'] ."/upload.php"; ?>',
    data: {lat:lat, long:long, foto:foto},
    success: function(result)
      {
      $("#berhasil").show();
      $("#tanggal").html(result.tanggal);
      $("#alamat").html(result.alamat);
      $("#response_status").show();
      $("#loader").hide();
      },
    error: function (xhr, ajaxOptions, thrownError)
      {
      alert('Timeout')
      }
    }
  );
}
</script>
<script src="js/imagecapture.js"></script>
<?php include('footer.php'); ?>
