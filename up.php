<?php  
$extension = explode('/', mime_content_type($_POST['foto']))[1];
$namafile = 'foto'. rand(111,999999) .'.'. $extension;
 
base64ToImage($_POST['foto'], $namafile);

function base64ToImage($base64_string, $output_file) {
    $file = fopen('storage/'. $output_file, "wb");

    $data = explode(',', $base64_string);

    fwrite($file, base64_decode($data[1]));
    fclose($file);

    return $output_file;
}

?>