<?php include('header.php'); ?>
<?php include('header_nav.php'); ?>

    <div class="page-content-wrapper">
      <!-- Hero Slides-->
      <div class="owl-carousel-one owl-carousel">
        <!-- Single Hero Slide-->
        <div class="single-hero-slide bg-overlay" style="background-image: url('img/bg-img/31.jpg')">
          <div class="slide-content h-100 d-flex align-items-center text-center">
            <div class="container">
              <h3 class="text-white" data-animation="fadeInUp" data-delay="100ms" data-wow-duration="500ms">Build with Bootstrap 5</h3>
              <p class="text-white mb-4" data-animation="fadeInUp" data-delay="400ms" data-wow-duration="500ms">Build fast, responsive sites with Bootstrap.</p><a class="btn btn-creative btn-warning" href="#" data-animation="fadeInUp" data-delay="800ms" data-wow-duration="500ms">Purchase Now</a>
            </div>
          </div>
        </div>
        <!-- Single Hero Slide-->
        <div class="single-hero-slide bg-overlay" style="background-image: url('img/bg-img/32.jpg')">
          <div class="slide-content h-100 d-flex align-items-center text-center">
            <div class="container">
              <h3 class="text-white" data-animation="fadeInUp" data-delay="100ms" data-wow-duration="1000ms">PWA Ready</h3>
              <p class="text-white mb-4" data-animation="fadeInUp" data-delay="400ms" data-wow-duration="1000ms">Make your website feel more like an app.</p><a class="btn btn-creative btn-warning" href="#" data-animation="fadeInUp" data-delay="800ms" data-wow-duration="500ms">Buy Now</a>
            </div>
          </div>
        </div>
        <!-- Single Hero Slide-->
        <div class="single-hero-slide bg-overlay" style="background-image: url('img/bg-img/33.jpg')">
          <div class="slide-content h-100 d-flex align-items-center text-center">
            <div class="container">
              <h3 class="text-white" data-animation="fadeInUp" data-delay="100ms" data-wow-duration="1000ms">Unique Elements &amp; Pages</h3>
              <p class="text-white mb-4" data-animation="fadeInUp" data-delay="400ms" data-wow-duration="1000ms">Create your website in minutes, not weeks.</p><a class="btn btn-creative btn-warning" href="#" data-animation="fadeInUp" data-delay="800ms" data-wow-duration="500ms">Purchase Now</a>
            </div>
          </div>
        </div>
      </div>
      <!-- Welcome Toast--><div class="toast toast-autohide custom-toast-1 toast-danger home-page-toast" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="10000" data-bs-autohide="true">
<div class="toast-body">
<svg width="30" height="30" viewBox="0 0 16 16" class="bi bi-bookmark-check text-white" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
<path fill-rule="evenodd" d="M10.854 5.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 7.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
</svg>
      <div class="toast-text ms-3 me-2">
        <p class="mb-1 text-white">Welcome to Affan template!</p><small class="d-block">Click the "Add to Home Screen" button & enjoy it like an app.</small>
      </div>
      <button class="btn btn-close btn-close-white position-relative p-1 ms-auto" type="button" data-bs-dismiss="toast" aria-label="Close"></button></div>
</div>
      <div class="affan-features-wrap py-3">
        <div class="container">
          <div class="row g-3">
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><img src="img/demo-img/bootstrap.png" alt="">
                  <h6 class="mb-0">Bootstrap 5</h6>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><img src="img/demo-img/pwa.png" alt="">
                  <h6 class="mb-0">PWA Ready</h6>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><img src="img/demo-img/sass.png" alt="">
                  <h6 class="mb-0">SCSS</h6>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><img src="img/demo-img/pug.png" alt="">
                  <h6 class="mb-0">Pug</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="card bg-danger mb-3 shadow-sm element-card wow fadeInUp" data-wow-duration="1s">
          <div class="card-body">
            <h2 class="text-white">Reusable elements</h2>
            <p class="text-white mb-4">More than 220+ reusable modern design elements. Just copy the code and paste it on your desired page.</p><a class="btn btn-light" href="elements.html">View all elements</a>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="card bg-primary shadow-sm page-card wow fadeInUp" data-wow-duration="1s">
          <div class="card-body">
            <h2 class="text-white">Ready pages</h2>
            <p class="text-white mb-4">Already designed more than 32+ pages for your needs. Such as - authentication pages, shop pages, blog &amp; miscellaneous pages.</p><a class="btn btn-light" href="pages.html">View all pages</a>
          </div>
        </div>
      </div>
      <div class="affan-features-wrap py-3">
        <div class="container">
          <div class="row g-3">
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><img src="img/demo-img/npm.png" alt="">
                  <h6 class="mb-0">npm</h6>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><img src="img/demo-img/gulp.png" alt="">
                  <h6 class="mb-0">Gulp 4</h6>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><svg width="34" height="34" viewBox="0 0 16 16" class="bi bi-box-arrow-left text-primary" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 0 .5.5h8a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5h-8a.5.5 0 0 0-.5.5v2a.5.5 0 0 1-1 0v-2A1.5 1.5 0 0 1 6.5 2h8A1.5 1.5 0 0 1 16 3.5v9a1.5 1.5 0 0 1-1.5 1.5h-8A1.5 1.5 0 0 1 5 12.5v-2a.5.5 0 0 1 1 0v2z"/>
<path fill-rule="evenodd" d="M.146 8.354a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L1.707 7.5H10.5a.5.5 0 0 1 0 1H1.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3z"/>
</svg>
                  <h6 class="mb-0">RTL Ready</h6>
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="card text-center shadow-sm wow fadeInUp" data-wow-duration="1s">
                <div class="card-body"><svg width="34" height="34" viewBox="0 0 16 16" class="bi bi-moon text-primary" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" d="M14.53 10.53a7 7 0 0 1-9.058-9.058A7.003 7.003 0 0 0 8 15a7.002 7.002 0 0 0 6.53-4.47z"/>
</svg>
                  <h6 class="mb-0">Dark Mode</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer Nav-->

<?php include('bottom_nav.php'); ?>
<?php include('footer.php'); ?>
