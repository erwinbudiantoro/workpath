<?php include('header.php'); ?>
    <!-- Hero Block Wrapper-->
    <div class="hero-block-wrapper bg-primary">
      <!-- Styles-->
      <div class="hero-block-styles">
        <div class="hb-styles1" style="background-image: url('img/core-img/dot.png')"></div>
        <div class="hb-styles2"></div>
        <div class="hb-styles3"></div>
      </div>

      <!-- Hero Block Content-->
      <div class="hero-block-content">
        <div align="center"><img class="mb-4" width="230" src="img/bg-img/19.png" alt=""></div>
        <h2 class="display-4 text-white mb-3"><small>Selamat datang di </small>Workpad</h2>
        <p class="text-white">Aplikasi reporting pekerjaan yang Terjadwal dan Transparan. <br>Dirancang khusus untuk anda para pekerja Profesional dan Supervisor.</p><a class="btn btn-warning btn-lg w-100" href="page-login.php">Masuk</a>
      </div>
    </div>
<?php include('footer.php'); ?>
