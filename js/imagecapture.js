
'use strict'; 
// window.isSecureContext could be used for Chrome
var isSecureOrigin = location.protocol === 'https:' ||
location.host === 'localhost';
if (!isSecureOrigin) {
  alert('getUserMedia() must be run from a secure origin: HTTPS or localhost.' +
    '\n\nChanging protocol to HTTPS');
  location.protocol = 'HTTPS';
}
var constraints;
var imageCapture;
var mediaStream;
var takePhotoButton = document.querySelector('button#takePhoto');
var img = document.querySelector('img');
var video = document.querySelector('video');

takePhotoButton.onclick = takePhoto;

// Get a list of available media input (and output) devices
// then get a MediaStream for the currently selected input device
navigator.mediaDevices.enumerateDevices()
  .then(gotDevices)
  .catch(error => {
    console.log('enumerateDevices() error: ', error);
  })
  .then(playcam);

// From the list of media devices available, set up the camera source <select>,
// then get a video stream from the default camera source.
function gotDevices(deviceInfos) {
  var cams = 0;
  for (var i = 0; i !== deviceInfos.length; ++i) {
    var deviceInfo = deviceInfos[i];
    var option = document.createElement('option');
    option.value = deviceInfo.deviceId;
    if (deviceInfo.kind === 'videoinput') {
      $('<input type="hidden" id="cam'+ cams +'" value="'+ deviceInfo.deviceId +'" />').prependTo("#listcam");;
      cams++;
    }
  }
  if(cams == 1)
    {
    $("#swipe_cam").remove();
    $("#take_pict").removeClass("take_pict");
    }
}

var activecam = 1;
function swipeCam()
  {
    var deviceId = $("#cam"+ activecam).val();
    selectCam(deviceId);
    if(activecam == 0)
        {
        activecam = 1;
        }
    else
        {
        activecam = 0;
        }
  }

// Get a video stream from the currently selected camera source.
function playcam() {
  if (mediaStream) {
    mediaStream.getTracks().forEach(track => {
      track.stop();
    });
  }
  var videoSource = $("#cam0").val();
  constraints = {
    video: { deviceId: videoSource ? {exact: videoSource} : undefined}
  };
  navigator.mediaDevices.getUserMedia(constraints)
    .then(gotStream)
    .catch(error => {
      console.log('getUserMedia error: ', error);
    });
}

function selectCam(videoSource) {
  if (mediaStream) {
    mediaStream.getTracks().forEach(track => {
      track.stop();
    });
  }
  constraints = {
    video: { deviceId: videoSource ? {exact: videoSource} : undefined}
  };
  navigator.mediaDevices.getUserMedia(constraints)
    .then(gotStream)
    .catch(error => {
      console.log('getUserMedia error: ', error);
    });
}

// Display the stream from the currently selected camera source, and then
// create an ImageCapture object, using the video from the stream.
function gotStream(stream) {
  mediaStream = stream;
  video.srcObject = stream;
  video.classList.remove('hidden');
  imageCapture = new ImageCapture(stream.getVideoTracks()[0]);
}


function stopStreamedVideo() {
  var videoElem = document.getElementById("vidStream");
  const stream = videoElem.srcObject;
  const tracks = stream.getTracks();

  tracks.forEach(function(track) {
    track.stop();
  });

  videoElem.srcObject = null;

}


// Get a Blob from the currently selected camera source and
// display this with an img element.
function takePhoto() {
  imageCapture.takePhoto().then(function(blob) {
    resizeImage(blob, 500, 500).then(blob => {
    	//You can upload the resized image: doUpload(blob)
      $("#take").hide();

      var reader = new FileReader();
       reader.readAsDataURL(blob);
       reader.onloadend = function() {
           var base64data = reader.result;
           document.getElementById('foto').value = base64data;
           document.getElementById("preview").innerHTML = '<img width="100%" src="'+ base64data +'">';
           stopStreamedVideo();
           $("#loader").show();
           sendData();
       }

    }, err => {
    	console.error("Photo error", err);
    });
  }).catch(function(error) {
    console.log('takePhoto() error: ', error);
  });
}

function resizeImage(file, maxWidth, maxHeight) {
    return new Promise((resolve, reject) => {
        let image = new Image();
        image.src = URL.createObjectURL(file);
        image.onload = () => {
            let width = image.width;
            let height = image.height;

            if (width <= maxWidth && height <= maxHeight) {
                resolve(file);
            }

            let newWidth;
            let newHeight;

            if (width > height) {
                newHeight = height * (maxWidth / width);
                newWidth = maxWidth;
            } else {
                newWidth = width * (maxHeight / height);
                newHeight = maxHeight;
            }

            let canvas = document.createElement('canvas');
            canvas.width = newWidth;
            canvas.height = newHeight;

            let context = canvas.getContext('2d');

            context.drawImage(image, 0, 0, newWidth, newHeight);

            canvas.toBlob(resolve, file.type);
        };
        image.onerror = reject;
    });
}
