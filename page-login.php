 <?php include('header.php'); ?>
    <!-- Back Button-->

    <!-- Login Wrapper Area-->
    <div class="login-wrapper d-flex align-items-center justify-content-center">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">
            <div class="text-center px-4"><img width="180" src="img/core-img/logo.png" alt=""><br><br></div>
            <!-- Register Form-->
            <div class="register-form mt-4 px-4">
              <form action="/workpath/page-home.php">
                <div class="form-group text-start mb-3">
                  <input class="form-control" type="text" placeholder="Email">
                </div>
                <div class="form-group text-start mb-3">
                  <input class="form-control" type="password" placeholder="Password">
                </div>
                <button class="btn btn-info w-100" type="submit">Log In</button>
              </form>
            </div>
            <!-- Login Meta-->


            <div class="login-meta-data row px-4 py-2">
              <div class="col-6"><a href="page-register.php">Register</a></div>
              <div class="col-6" align="right"><a href="page-forget-password.php">Lupa password</a></div>
            </div>

            <div class="row px-4 py-2">
              <div class="grid"><a class="btn m-1 btn-creative btn-primary" href="#"><i class="fa fa-facebook me-2"></i>Facebook</a></div>
              <div class="grid"><a class="btn m-1 btn-creative btn-danger" href="#"><i class="fa fa-google me-2"></i>Google</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php include('footer.php'); ?>
