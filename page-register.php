<?php include('header.php'); ?>
    <!-- Back Button-->
    <div class="login-back-button"><a href="page-login.html"><svg width="32" height="32" viewBox="0 0 16 16" class="bi bi-arrow-left-short" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
</svg></a></div>
    <!-- Login Wrapper Area-->
    <div class="login-wrapper d-flex align-items-center justify-content-center">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-sm-9 col-md-7 col-lg-6 col-xl-5">
            <div class="text-center px-4"><img class="login-intro-img" src="img/bg-img/36.png" alt=""></div>
            <!-- Register Form-->
            <div class="register-form mt-4 px-4">
              <form action="https://designing-world.com/affan-1.0/page-otp.html">
                <div class="form-group text-start mb-3">
                  <input class="form-control" type="text" placeholder="Email address">
                </div>
                <div class="form-group text-start mb-3">
                  <input class="form-control" type="text" placeholder="Username">
                </div>
                <div class="form-group text-start mb-3">
                  <input class="form-control input-psswd" id="registerPassword" type="password" placeholder="New Password">
                </div>
                <div class="form-check mb-3">
                  <input class="form-check-input" id="checkedCheckbox" type="checkbox" value="" checked>
                  <label class="form-check-label" for="checkedCheckbox">I agree with the terms &amp; privacy policy.</label>
                </div>
                <button class="btn btn-primary w-100" type="submit">Sign Up</button>
              </form>
            </div>
            <!-- Login Meta-->
            <div class="login-meta-data text-center">
              <p class="mt-3 mb-0">Already have an account?<a class="ms-1 stretched-link" href="page-login.html">Login</a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php include('footer.php'); ?>
