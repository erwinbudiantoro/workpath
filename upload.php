<?php

$extension = explode('/', $_POST['foto']);
$extension2 = explode(';base64', $extension[1]);

$namafile = 'foto'. rand(111,999999) .'.'. $extension2[0];

base64ToImage($_POST['foto'], $namafile);

function base64ToImage($base64_string, $output_file) {
    $file = fopen('storage/'. $output_file, "wb");

    $data = explode(',', $base64_string);

    fwrite($file, base64_decode($data[1]));
    fclose($file);

    return $output_file;
}

function getaddress($lat, $long)
  {
  $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='. $lat .','. $long .'&sensor=false&key=AIzaSyAaTa7bmxh-GrtnXk5QHyaUFK0EtgqD5Z4';
  $json = @file_get_contents($url);
  $data=json_decode($json);
  $status = $data->status;
  if($status=="OK")
    {
      return $data->results[0]->formatted_address;
    }
  else
    {
      return false;
    }
  }
$getaddress = getaddress($_POST['lat'], $_POST['long']);

$return['face_status'] = 1;
$return['status'] = 1;

if($getaddress == false)
    {
    $return['alamat'] = '';
    $return['alamat_status'] = 0;
    }
else
    {
    $return['alamat'] = $getaddress;
    $return['alamat_status'] = 1;
    }
$return['tanggal'] = date('d M Y H:i');

echo json_encode($return);
?>
