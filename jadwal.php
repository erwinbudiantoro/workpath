<?php include('header.php'); ?>
<?php include('header_nav.php'); ?>
    <div id="setting-popup-overlay"></div>
    <!-- Setting Popup Card-->
    <div class="card setting-popup-card shadow-lg" id="settingCard">
      <div class="card-body">           
        <div class="container">
          <div class="setting-heading d-flex align-items-center justify-content-between mb-3">
            <p class="mb-0"> </p><a class="btn-close" id="settingCardClose" href="#"></a>
          </div> 
            <div class="mb-1"> 
              <label for="fromDate"><small>Dari Tanggal</small></label>
			  <input class="form-control" type="date" value="<?php echo date('Y-m-d'); ?>" id="fromDate" />
            </div> 
			<div class="mb-2"> 
              <label for="fromDate"><small>Sampai</small></label>
			  <input class="form-control" type="date" value="<?php echo date('Y-m-d'); ?>" id="fromDate" />
            </div> 
           <div class="mb-2"> 
             <a class="btn w-100 btn-info" href="#">
              <svg width="16" height="16" viewBox="0 0 16 16" class="bi bi-cursor me-2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" d="M14.082 2.182a.5.5 0 0 1 .103.557L8.528 15.467a.5.5 0 0 1-.917-.007L5.57 10.694.803 8.652a.5.5 0 0 1-.006-.916l12.728-5.657a.5.5 0 0 1 .556.103zM2.25 8.184l3.897 1.67a.5.5 0 0 1 .262.263l1.67 3.897L12.743 3.52 2.25 8.184z"></path>
				</svg> Apply</a>
            </div> 
           
        </div>
      </div>
    </div>
    <!-- Header Area-->
    <div class="header-area" id="headerArea">
      <div class="container">
        <!-- Header Content-->
        <div class="header-content position-relative d-flex align-items-center justify-content-between">
          <!-- Back Button-->
          <div class="back-button"><a href="#" onclick="back()"><svg width="32" height="32" viewBox="0 0 16 16" class="bi bi-arrow-left-short" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
</svg></a></div>
          <!-- Page Title-->
          <div class="page-heading">
            <h6 class="mb-0">Jadwal</h6>
          </div>
          <!-- Settings-->
          <div class="setting-wrapper"><a class="setting-trigger-btn" id="settingTriggerBtn" href="#"><i class="fa fa-calendar"></i></a></div>
        </div>
      </div>
    </div>
    <div class="page-content-wrapper py-3">
      <div class="container">
        <!-- Element Heading-->
        <div class="element-heading">
          <h6><b>Selasa,</b> 26 September 2021</h6>
        </div>
      </div>
      <div class="container  mb-3">
        <!-- Timeline Content-->
        <div class="card timeline-card">
          <div class="card-body">
		  <div class="jadwal-jam mb-1"><i class="fa fa-clock-o"></i> 08:00 AM - 14:05 PM</div>
		  <div class="d-flex justify-content-between">
              <div  class="timeline-text mb-1">
				<div class="badge ml_min_set mb-1 rounded-pill">PT Gamatechno Indonesia</div>  
              </div>   
            </div>
			 <div class="timeline-text">  
				<div class="jadwal-text  mb-2">Visit dan cek kelengkapan pekerja sesuai dengan SOP</div>
				<div  class="jadwal-alamat"> <i class="fa fa-map-marker"></i> Jl Imogiri Km 13, Karangsemut, Trimulyo</div>	
			 </div>  
			 
          </div>
        </div>
        </div>
		<div class="container  mb-3">
        <!-- Timeline Content-->
        <div class="card timeline-card">
          <div class="card-body">
		  <div class="jadwal-jam mb-1"><i class="fa fa-clock-o"></i> 08:00 AM - 14:05 PM</div>
		  <div class="d-flex justify-content-between">
              <div  class="timeline-text mb-1">
				<div class="badge ml_min_set mb-1 rounded-pill">PT Gamatechno Indonesia</div>  
              </div>   
            </div>
			 <div class="timeline-text">  
				<div class="jadwal-text  mb-2">Visit dan cek kelengkapan pekerja sesuai dengan SOP</div>
				<div  class="jadwal-alamat"> <i class="fa fa-map-marker"></i> Jl Imogiri Km 13, Karangsemut, Trimulyo</div>	
			 </div>  
			 
          </div>
        </div>
        </div>
		<div class="container  mb-4">
        <!-- Timeline Content-->
        <div class="card timeline-card">
          <div class="card-body">
		  <div class="jadwal-jam mb-1"><i class="fa fa-clock-o"></i> 08:00 AM - 14:05 PM</div>
		  <div class="d-flex justify-content-between">
              <div  class="timeline-text mb-1">
				<div class="badge ml_min_set mb-1 rounded-pill">PT Gamatechno Indonesia</div>  
              </div>   
            </div>
			 <div class="timeline-text">  
				<div class="jadwal-text  mb-2">Visit dan cek kelengkapan pekerja sesuai dengan SOP</div>
				<div  class="jadwal-alamat"> <i class="fa fa-map-marker"></i> Jl Imogiri Km 13, Karangsemut, Trimulyo</div>	
			 </div>  
			 
          </div>
        </div>
        </div>
		
		
		
	 <div class="container">
        <!-- Element Heading-->
        <div class="element-heading">
          <h6><b>Rabu,</b> 27 September 2021</h6>
        </div>
      </div>
      <div class="container mb-3">
        <!-- Timeline Content-->
        <div class="card timeline-card">
          <div class="card-body">
		  <div class="jadwal-jam mb-1"><i class="fa fa-clock-o"></i> 08:00 AM - 14:05 PM</div>
		  <div class="d-flex justify-content-between">
              <div  class="timeline-text mb-1">
				<div class="badge ml_min_set mb-1 rounded-pill">PT Gamatechno Indonesia</div>  
              </div>   
            </div>
			 <div class="timeline-text">  
				<div class="jadwal-text  mb-2">Visit dan cek kelengkapan pekerja sesuai dengan SOP</div>
				<div  class="jadwal-alamat"> <i class="fa fa-map-marker"></i> Jl Imogiri Km 13, Karangsemut, Trimulyo</div>	
			 </div>  
			 
          </div>
        </div>
        </div>
		
		
		<div class="container">
        <!-- Element Heading-->
        <div class="element-heading">
          <h6><b>Kamis,</b> 28 September 2021</h6>
        </div>
      </div>
      <div class="container mb-3">
        <!-- Timeline Content-->
        <div class="card timeline-card">
          <div class="card-body">
		  <div class="jadwal-jam mb-1"><i class="fa fa-clock-o"></i> 08:00 AM - 14:05 PM</div>
		  <div class="d-flex justify-content-between">
              <div  class="timeline-text mb-1">
				<div class="badge ml_min_set mb-1 rounded-pill">PT Gamatechno Indonesia</div>  
              </div>   
            </div>
			 <div class="timeline-text">  
				<div class="jadwal-text  mb-2">Visit dan cek kelengkapan pekerja sesuai dengan SOP</div>
				<div  class="jadwal-alamat"> <i class="fa fa-map-marker"></i> Jl Imogiri Km 13, Karangsemut, Trimulyo</div>	
			 </div>  
			 
          </div>
        </div>
        </div>
		
    </div>
	
 
 <?php include('bottom_nav.php'); ?>
<?php include('footer.php'); ?>
